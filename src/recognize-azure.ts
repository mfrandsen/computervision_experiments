import { recognize } from "./recognize";
import _ from "lodash";
import { peak } from "./utils";

type WordResult = {
    boundingBox: string;
    text: string;
}

type RegionResult = {
    boundingBox: number;
    lines: { boundingBox: number, words: WordResult[] }[]
}

type Result = {
    language: 'da';
    orientation: string;
    textAngle: number;
    regions: RegionResult[];
}

type Options = { subscriptionKey: string };

const readText = (feature: Feature, options: Options) => {
    /// https://docs.microsoft.com/da-dk/azure/cognitive-services/Computer-vision/quickstarts/javascript

    return new Promise<TextLabel>(async (resolve, reject) => {

        const uriBase = "https://northeurope.api.cognitive.microsoft.com/vision/v1.0/ocr";

        const params = {
            "language": "da",
            "detectOrientation ": "false",
        };

        const response = await fetch(new Request(uriBase + "?" + $.param(params), {
            headers: {
                "Content-Type": "application/octet-stream",
                "Ocp-Apim-Subscription-Key": options.subscriptionKey,
            },
            method: "POST",
            body: await new Promise<Blob | null>((resolve) => feature.toBlob(resolve))
        }));

        const result = await response.json() as Result;
        if (!response.ok)
            throw new Error(response.status + " " + response.statusText + " " + result);

        const textResult = _.flatten(result.regions.map(x => x.lines.map(x => x.words.map(x => x.text).join(" ")))).join(" | ");

        resolve({
            result: { text: textResult } as any,
            confidence: NaN,
            canvas: feature
        });
    });
};

const readTexts = (featureSet: FeatureGroup, options: Options) =>
    Promise.all(featureSet.map(x => readText(x, options)));

export const recognizeTextSets = <TFeatures extends FeatureGroups>(features: TFeatures, options: KeyMap<TFeatures, Options>) => {
    console.log("recognize-azure.ts recognize text", Object.keys(features).map(key => `key=${key} (${features[key].length})`).join(", "));
    return recognize(readTexts, features, options).then(peak(() => console.log("recognize.ts recognize text DONE")));
}