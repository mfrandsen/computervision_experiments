import '../styles/main.sass';
import _ from 'lodash';
import { trackUntil } from "./track";
import { recognizeTextSets, barcodeRecognizer, unzipLabelDict } from "./recognize";
import { recognizeTextSets as azureRecognizeTextSets } from "./recognize-azure";
import { renderVideoFeed, renderFeatures, renderControlPanel, renderSaveBtn, renderLabels } from './render';
import { clean } from './clean';
import { extract } from './extract';
import { debugResults, debugWords, logConfidence } from './debug';
import { isBarcodeEnabled, isTextEnabled, isTestModeEnabled, isAzureTextEnabled } from './flags';
import { load } from './load';
import * as constants from './constants';
import * as config from './config';
import { targetRatioWithoutBarcode } from './constants';

export const sampleSize = 5;
export const sampleSubsetForAnalysis = 2;

window.onload = async () => {

  const { environment, subscriptionKey } = window.secrets;

  if (!['local', 'azure'].includes(environment))
    throw new Error("Unknown environment: " + environment);
 
  $('#content')
    .empty()
    .append(renderVideoFeed());

  $('aside')
    .append(renderControlPanel(environment));

  const samples = isTestModeEnabled()
    ? await Promise.all(_.flatten(['raw'].map((folder) => _.range(0, 2).map(file => load(`test-cases/${folder}/${file}.png`)))))
    : await trackUntil({
      sampleSize: sampleSize,
      selector: '#content',
      trackColors: ['yellow'],
      leastPixels: 65000,
      targetRatio: constants.targetRatioWithoutBarcode,
      ratioBound: 0.01
    });

  const features = extract(samples, { top: sampleSubsetForAnalysis, targetRatio: constants.targetRatioWithoutBarcode }); // TODO: try to use blob over canvas
  const textFeatures = Object.assign({}, features);
  delete textFeatures.barcode;

  const barcodeFeatures = { barcode: features.barcode };

  $('#content')
    .empty()
    .append(`<p>Captured ${samples.length} screenshots of which we took the best ${sampleSubsetForAnalysis} and has begun analyzing ${Object.keys(features).length} sub-images of each. Please wait while analysing data...</p>`);

  if (isBarcodeEnabled()) {
    console.log('performing ocr on barcodes.')
    const start = new Date();
    const results = await barcodeRecognizer(barcodeFeatures, config.recognizeOptions);

    $('#content').empty().append(renderLabels(results));

    const { images, text } = unzipLabelDict(results);
    $('.toggle-btn-container').append(renderSaveBtn('barcode results', images, text));
  }

  if (isTextEnabled()) {
    console.log('performing ocr on text features.')
    const start = new Date();
    const results = clean(await recognizeTextSets(textFeatures, config.recognizeOptions));

    if (!isBarcodeEnabled) $('#content').empty(); //  arcode block clears warning
    $('#content').append(renderLabels(results));

    const { images, text } = unzipLabelDict(results);
    $('.toggle-btn-container').append(renderSaveBtn('text results', images, text));

    logConfidence(results);
  }

  if (isAzureTextEnabled()) {
    console.log('performing ocr on text features.')
    const start = new Date();

    const results = await azureRecognizeTextSets({ azure_details: textFeatures.details, azure_raw: textFeatures.raw }, { azure_details: { subscriptionKey }, azure_raw: { subscriptionKey } });

    if (!isBarcodeEnabled && !isTextEnabled()) $('#content').empty();
    $('#content').append(renderLabels(results));

    const { images, text } = unzipLabelDict(results);
    $('.toggle-btn-container').append(renderSaveBtn('text results', images, text));

    logConfidence(results);
  }

  if (!isTextEnabled() && !isBarcodeEnabled() && !isAzureTextEnabled()) {
    console.log('just rendering captured images')
    $('#content').empty()
      .append(renderFeatures(textFeatures))
      .append(renderFeatures(barcodeFeatures));
  }

  $('.toggle-btn-container').append(renderSaveBtn('raw images',
    Object.entries(features).map(([name, canvases]) => ({ name, content: canvases })), {}));
};