import _ from "lodash";
import { getMedian } from "./utils";

export const logConfidence = (results: LabelGroups) =>
  Object.entries(results).forEach(([key, values]) =>
    console.debug(
      "key=", key,
      "avg confidence=", values.map(v => v.confidence).reduce((a, b) => a + b, 0) / values.length,
      "max confidence=", Math.max(...values.map(v => v.confidence)),
      "min confidence=", Math.min(...values.map(v => v.confidence)),
    ));

/**
 * These are just some temporary data exploration scripts, please ignore them. 
 */
export const debugWords = (key: string, results: TextLabel[]) => {
  console.group("Word analysis", key);
  const all = results.map(x => x.result!.words);

  const median = getMedian(all.map(x => x.length));
  console.log('median', median);

  const cleaned = all.filter(x => x.length === median);
  const zipped = _.zip(...cleaned) as Tesseract.Word[][];
  const sorted = zipped.map(x => x.slice().sort((a, b) => b.confidence - a.confidence));

  if (sorted.length == 0) return;
  if (sorted[0].length >= 1) sorted.map(x => x[0]).forEach((x, i) => console.log("#1 best " + i + 1 + " word:", x.text + " (" + x.confidence + ")"));
  if (sorted[0].length >= 2) sorted.map(x => x[1]).forEach((x, i) => console.log("#2 best " + i + 1 + " word:", x.text + " (" + x.confidence + ")"));
  if (sorted[0].length >= 3) sorted.map(x => x[2]).forEach((x, i) => console.log("#3 best " + i + 1 + " word:", x.text + " (" + x.confidence + ")"));
  if (sorted[0].length >= 4) sorted.map(x => x[3]).forEach((x, i) => console.log("#4 best " + i + 1 + " word:", x.text + " (" + x.confidence + ")"));
  if (sorted[0].length >= 5) sorted.map(x => x[4]).forEach((x, i) => console.log("#5 best " + i + 1 + " word:", x.text + " (" + x.confidence + ")"));
}

export const debugResults = <T>(start: Date, results: LabelGroups) => {
  console.group('Debug: Time analyzing', new Date().getTime() - start.getTime(), 'ms');
  for (const [key, values] of Object.entries(results)) {
    console.group('Debug', key);
    if (values.length === 0) {
      console.log('No valid results where found.');
    } else {
      const best = values.sort((a, b) => b.confidence - a.confidence)[0];
      console.log("Max confidence", best.confidence, best.result && best.confidence);
      console.log('Average confidence: ', values.map(x => x.confidence).reduce((a, b) => a + b) / values.length);
    }
    console.groupEnd();
  }
  console.groupEnd();
}
