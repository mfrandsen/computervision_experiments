import { getRatio } from './utils';

const barcodeHeight = 170; // additional height because barcode reader gets confused if there's nothing below the barcode. 
const target = { width: 542, height: 217+barcodeHeight, barcodeHeight: barcodeHeight };
export const targetRatioWithoutBarcode = getRatio({ width: 542, height: 217 });

// convert to percentage of target
const relative = (r: Rectangle) => ({
    x: r.x / target.width,
    y: r.y / target.height,
    width: r.width / target.width,
    height: r.height / target.height
});

export const sizeFactors = {
    barcodeHeight: target.barcodeHeight / (target.height - target.barcodeHeight),
    raw: { x: 0, y: 0, width: 1, height: 1 },
    cpr: relative({ x: 0, y: 70, width: 250, height: 35 }),
    validFrom: relative({ x: 420, y: 80, width: 130, height: 35 }),
    details: relative({ x: 0, y: 100, width: 390, height: 115 }),
    line1: relative({ x: 0, y:100, width: 390, height: 33 }),
    line2: relative({ x: 0, y: 130, width: 390, height: 33 }),
    line3: relative({ x: 0, y: 155, width: 390, height: 33 }),
    line4: relative({ x: 0, y: 185, width: 390, height: 33 }),
    
    // NOTE: this crop factor is to high, because the barcode reader likes to have some extra pixels around the target
    barcode: relative({ x: 70, y: 200, width: 390, height: 170 }) 
};