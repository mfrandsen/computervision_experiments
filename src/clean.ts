import _ from "lodash";

const pipe = <T>(f: (value: T) => void) => (x: T) => { f(x); return x; }

const cleanCpr = (results: TextLabel[]) => 
  results
    .map(x => { x.result.text = x.result!.text.replace(/\s/g, ''); return x })
    .filter(x => x.result!.text.length === 11 && x.result!.text[6] === '-');

export const clean = (input: LabelGroups & { cpr: TextLabel[] }) => {
    const output = _.cloneDeep(input);
  
    output.cpr = cleanCpr(output.cpr);

    console.groupCollapsed(`Cleaning. Left are ${output.cpr.length} valid cpr results out of ${input.cpr.length}.`);
    console.log('Cleaning done.')
    console.groupEnd();
  
    return output;
  }