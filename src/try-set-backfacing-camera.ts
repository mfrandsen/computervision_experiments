// https://stackoverflow.com/questions/18483160/which-camera-will-open-getusermedia-api-in-mobile-device-front-or-rear½
// https://stackoverflow.com/questions/18625007/enable-rear-camera-with-html5
// https://www.html5rocks.com/en/tutorials/getusermedia/intro/

export const trySetBackfacingCamera = async (video: HTMLVideoElement) => {
    const videoDevices = (await navigator.mediaDevices.enumerateDevices())
        .filter(x => x.kind === 'videoinput');

    if (videoDevices.length > 1) { 
        const device = videoDevices[1];
        const constraints = { deviceId: { exact: device.deviceId } };

        video.srcObject = await navigator.mediaDevices.getUserMedia({ video: constraints })
    }
}