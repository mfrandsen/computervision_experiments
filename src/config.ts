const psm = {
    singletextline: 7,
    sparseText: 11, 
    sparseTextWithOsd: 12, 
    singleUniformTextBlock: 6,
    rawline: 13,
}

// notes from experiments
// See https://github.com/naptha/tesseract.js/blob/master/docs/tesseract_parameters.md
// export const recognizeOptions = {
//     barcode: {
//         decoder: { readers: ["code_128_reader"] },
//         locate: true,
//         locator: { patchSize: 'medium',  halfSample: true }
//     },
//     // raw: {}, // avg confidence= 58.7, 0/*
//     raw: { lan: 'dan',}, // avg confidence= 58.7 0/*
//     cpr: {}, // avg confidence= 78.375, 9/9
//     // cpr: { psm: psm.singletextline, tessedit_char_whitelist: '0123456789-' }, // avg confidence= 78.33, 9/9
//     // name: { lan: 'dan' },
//     // line1: { lan: 'dan' },
//     // line2: { lan: 'dan' },
//     // line3: { lan: 'dan' },
//     // line4: { lan: 'dan' }, // avg confidence= 75.7, 0/9, letter part 7/9, might be improved by separating number and letters
//     // line4: { lan: 'dan', psm: psm.rawline }, // avg confidence = 75.7, 7/9
//     // line4: { lan: 'dan', psm: psm.singletextline }, // avg confidence= 76
//     line4: { lan: 'dan', psm: psm.rawline }, // // avg confidence= 76
//     // details: { lan: 'dan' }, // avg confidence= 72.7, 0/9, multiple subhits
//     // details: { lan: 'dan', psm: psm.sparseText }, // confidence= 72.7
//     // details: { lan: 'dan', psm: psm.sparseTextWithOsd }, // avg confidence= 72.7
//     details: { lan: 'dan', psm: psm.singleUniformTextBlock }, // avg confidence= 72
//     // validFrom: { } // avg confidence= 6.3, 0/0, 
//     // validFrom: { psm: psm.singletextline, tessedit_char_whitelist: '0123457789.' } // avg confidence= 1.7
// }

export const recognizeOptions = {
    barcode: { // consistently working well
        decoder: { readers: ["code_128_reader"] },
        locate: true,
        locator: { patchSize: 'medium'}
    },
    raw: { lan: 'dan', psm: psm.sparseTextWithOsd }, 
    cpr: { psm: psm.singletextline, tessedit_char_whitelist: '0123456789-' }, // consistently working well
    name: { lan: 'dan', psm: psm.singletextline }, // some results, but not good for any actual use
    line1: { lan: 'dan', psm: psm.singletextline }, // some results, but not good for any actual use
    line2: { lan: 'dan', psm: psm.singletextline }, // some results, but not good for any actual use
    line3: { lan: 'dan', psm: psm.singletextline }, // some results, but not good for any actual use
    line4: { lan: 'dan', psm: psm.singletextline }, // some results, but not good for any actual use
    details: { lan: 'dan', psm: psm.singleUniformTextBlock }, // // some results, but not good for any actual use, possible better results by looking at each individual result line
    validFrom: { psm: psm.singletextline, tessedit_char_whitelist: '0123457789.' } // consistently too small to be read 
}