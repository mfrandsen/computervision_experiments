import * as flags from "./flags";
import { saveToDisk } from "./save";
import * as utils from './utils';
import * as extract from "./extract";

type NormalisedLabel = { text: string, confidence: number, canvas: HTMLCanvasElement };
const normalize = (labels: StringMap<Label[]>) =>
    Object.entries(labels)
        .map(([name, values]) => ({
            name,
            content: values.map(x => ({
                confidence: x.confidence,
                canvas: x.canvas,
                text: utils.labelToString(x)
            }))
        }))

export const renderSaveBtn = (folderName: string, imageGroups: ImageGroup[], about: Object) =>
    $(`<button>save ${folderName}</button>`).click(() => {
        if (confirm("Do you wish to save the data files to disk?")) {
            saveToDisk(folderName, imageGroups, about);
        }
    });

export const renderControlPanel = (environment: 'azure' | 'local') => {
    const container = $(`<div class='toggle-btn-container'></div>`)
        .append($(`<label><input type="checkbox" ${flags.isTextEnabled() ? 'checked' : ''}/>parse text</label>`).change(flags.toggleParseText)).append('<br />')
        .append($(`<label><input type="checkbox" ${flags.isAzureTextEnabled() ? 'checked' : ''}/>parse text via azure service</label>`).change(flags.toggleAzureParseText)).append('<br />')
        .append($(`<label><input type="checkbox" ${flags.isBarcodeEnabled() ? 'checked' : ''}/>parse barcodes</label>`).change(flags.toggleParseBarcodes)).append('<br />');

        if (environment === 'local')
            container.append($(`<label><input type="checkbox" ${flags.isTestModeEnabled() ? 'checked' : ''}/>test mode</label>`).change(flags.toggleTestMode)).append('<br />')
        
        container.append($(`<button>reload</button>`).click(flags.reload)).append('<br />');

        return container;
}

export const reRenderBoundaryOnCanvas = (rect: Rectangle, canvas: HTMLCanvasElement, snapshots: number, leastPixels: number, targetRatio: number, quality: TrackQuality) => {
    const context = canvas.getContext('2d')!;
    context.clearRect(0, 0, canvas.width, canvas.height)

    context.strokeStyle = quality == "Good"
        ? 'green'
        : quality == "BadSize" ? 'yellow'
            : quality == 'BadRatio' ? 'orange'
                : 'red';

    context.lineWidth = 2;
    context.font = '11px Helvetica';
    context.fillStyle = "#fff";
    context.strokeRect(rect.x, rect.y, rect.width, rect.height);

    const xOffset = rect.x + rect.width - 120;
    let yOffset = rect.y + 11;

    const write = (...msgs: string[]) => context.fillText(msgs.join(' '), xOffset, (yOffset += 11));
    write('height:      ' + rect.height + 'px');
    write('width:       ' + rect.width + 'px');
    write('re:          ' + utils.getRatioDeviationFromTarget(rect, targetRatio) + '%');
    write('size:        ' + utils.getPixels(rect).toLocaleString('da-DA') + '/' + leastPixels.toLocaleString('da-DA') + 'px');
    write('snapshots :  ' + snapshots);
}

export const renderVideoFeed = () =>
    $(` 
        <section id='video-feed'>
            <video preload autoplay loop muted controls />
            <canvas />
        </section>`);


export const renderLabels = (labelMap: StringMap<Label[]>) => {
    const renderLabel = (key: string, data: NormalisedLabel) => {
        const result = $('<div />')
            .addClass('result')
            .append(data.canvas)
            .append($('<p />')
                .append(`<span class='confidence'>Confidence: ${data.confidence}%</span>`)
                .append('<br />')
                .append(`<span class='confidence'>Text: ${data.text}</span>`));

        if (!flags.isGroupVisible(key))
            result.hide();

        return result;
    }

    const renderGroup = (key: string, group: NormalisedLabel[]) =>
        $('<section />').click(function (this: any) { $(this).find('.result').toggle() })
            .addClass('result-group')
            .append($(`<h2>${key} (${group.length})</h2>`))
            .append(group.map(x => renderLabel(key, x)));

    return $('<div />')
        .attr('id', 'results')
        .append(normalize(labelMap).map(x =>
            renderGroup(x.name, x.content)));
}

export const renderFeatures = (groups: FeatureGroups) => {
    const renderFeature = (key: string, feature: Feature) => {
        const result = $('<div />')
            .addClass('result')
            .append(feature);

        if (!flags.isGroupVisible(key))
            result.hide();

        return result;
    }

    const renderGroup = (key: string, group: FeatureGroup) =>
        $('<section />').click(function (this: any) { $(this).find('.result').toggle(); flags.toggleGroup(key) })
            .addClass('result-group')
            .attr('id', `result-group-for-${key}`)
            .append($(`<h2>${key} (${group.length})</h2>`))
            .append(group.map(x => renderFeature(key, x)));

    return $('<div />')
        .attr('id', 'results')
        .append(Object.entries(groups).map(([key, values]) =>
            renderGroup(key, values)));
}