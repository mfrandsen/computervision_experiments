import { crop, getRatioDeviationFromTarget, getPixels } from "./utils";
import { sizeFactors } from "./constants";

export const extract = (images: HTMLCanvasElement[], options: { top: number, targetRatio: number }) => {
  
  // some arbitrary distance function to decide which samples are better,
  // should account for both size and ratio
  const samples = images
    .sort((a, b) => 
        ((getPixels(a) - getPixels(b)) / getPixels(b))**2 + 
          (getRatioDeviationFromTarget(a, options.targetRatio) - getRatioDeviationFromTarget(b, options.targetRatio)) / getRatioDeviationFromTarget(b, options.targetRatio)**2)
        // .sort((a, b) => getRatioDeviationFromTarget(a, options.targetRatio) - getRatioDeviationFromTarget(b, options.targetRatio))
    .slice(0, options.top);

  const curriedCrop = (sizeFactors: Rectangle) => (image: HTMLCanvasElement) => {
    const x = Math.min(image.width * sizeFactors.x, image.width);
    const y = Math.min(image.height * sizeFactors.y, image.height);
    const width = Math.min(image.width * sizeFactors.width, image.width - x);
    const height = Math.min(image.height * sizeFactors.height, image.height - y); 
    return crop(image, { x, y, width, height });
  }

  return {
    raw: samples,
    cpr: samples.map(curriedCrop(sizeFactors.cpr)),
    line1: samples.map(curriedCrop(sizeFactors.line1)),
    line2: samples.map(curriedCrop(sizeFactors.line2)),
    line3: samples.map(curriedCrop(sizeFactors.line3)),
    line4: samples.map(curriedCrop(sizeFactors.line4)),
    details: samples.map(curriedCrop(sizeFactors.details)),
    validFrom: samples.map(curriedCrop(sizeFactors.validFrom)),
    barcode: samples.map(curriedCrop(sizeFactors.barcode)),
  };
}