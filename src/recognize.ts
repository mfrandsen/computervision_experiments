import { labelToString, peak } from "./utils";

const readBarcode = async (feature: Feature, options: QuaggaJSConfigObject) =>
    new Promise<BarcodeLabel>(resolve => {
            Quagga.decodeSingle(Object.assign(options, {
                src: feature.toDataURL('image/jpeg', 1.0) // TODO: maybe send blobs around instead?
            }), result => resolve({ confidence: result && result.codeResult ? 100 : 0, canvas: feature, result }));
        });

const readBarcodes = async (featureSet: FeatureGroup, options: QuaggaJSConfigObject) => 
    Promise.all(featureSet.map(x => readBarcode(x, options)));

const readText = async (feature: Feature, options: TesseractOptions) => 
    new Promise<TextLabel>(resolve => 
            Tesseract.recognize(feature, options) // TODO: this is really slow! =O It was faster before!...
            .then(result => resolve({ canvas: feature, confidence: result.confidence, result })));

const readTexts = async (featureSet: FeatureGroup, options: TesseractOptions) =>  
    Promise.all(featureSet.map(x => readText(x, options)));


export const recognize = async <TFeatures extends FeatureGroups, TOptions, TLabel> (
    recognizer: Recognizer<TLabel, TOptions>, 
    features: TFeatures,
    options: RecognizerOptionsMap<TFeatures, TOptions>): Promise<KeyMap<TFeatures, TLabel[]>> => {

    const resultGroups = await Promise.all(Object.entries(features)
        .map(([key, set]) => {
            if (!(key in options)) throw new Error(`missing options object for recognizer ${key}`)
            return ({ key, results: recognizer(set, options[key]) })
        })
        .map(async x =>  ({ key: x.key, results: await x.results })));

    return resultGroups.reduce(
        (acc, x) => { acc[x.key] = x.results; return acc; }, 
        {} as KeyMap<TFeatures, TLabel[]>);
}

export const barcodeRecognizer = <TFeatures extends FeatureGroups>(features: TFeatures, options: KeyMap<TFeatures, QuaggaJSConfigObject>) => {
    console.log("recognize.ts recognize barcode", Object.keys(features).map(key => `key=${key} (${features[key].length})`).join(", "));
    return recognize(readBarcodes, features, options).then(peak(() => console.log("recognize.ts recognize barcode DONE")));
}

export const recognizeTextSets = <TFeatures extends FeatureGroups>(features: TFeatures, options: KeyMap<TFeatures, TesseractOptions>) => {
    console.log("recognize.ts recognize text", Object.keys(features).map(key => `key=${key} (${features[key].length})`).join(", "));
    return recognize(readTexts, features, options).then(peak(() => console.log("recognize.ts recognize text DONE")));
}

export const unzipLabelDict = (groups: LabelGroups) => {
    const images = Object
        .entries(groups)
        .map(([name, canvases]) => ({ 
            name, 
            content: canvases.map(x => x.canvas) }));

    const text = Object
        .entries(groups)
        .map(([key, values], i) => ({ 
            index: i, 
            key: key,
            values: values.map(r => ({ 
                confidence: r.confidence, 
                result: labelToString(r)
            }))
        }));

    return { images, text };
}