import * as utils from './utils';
import { renderVideoFeed, reRenderBoundaryOnCanvas } from './render';
import { sizeFactors } from './constants';
import { sortBySize, getQuality, scaleBy } from './utils';
import { trySetBackfacingCamera } from './try-set-backfacing-camera';


/**
 * Render a video stream connected to the users webcam, then tracks all yellow rectangle 
 * recognized in the stream. Each time there's a hit, the largest rectangle with the ratio
 * most similar to a target ratio is accumulated.
 * 
 * When {sampleSize} results have been sampled, the cropped images are returned.
 */

export async function trackUntil(options: {
  selector: string,
  sampleSize: number,
  trackColors: string[],
  leastPixels: number,
  ratioBound: number,
  targetRatio: number
}) {
  console.group('Track Start.');

  const wrapper = $(options.selector);
  if (!wrapper.has('video') || !wrapper.has('canvas'))
    throw new Error(`Misconfigured container, ${options.selector} should contain a video and a canvas element.`);
  
  const video = wrapper.find('video')[0] as HTMLVideoElement;
  const canvas = wrapper.find('canvas')[0] as HTMLCanvasElement;
  const tracker = new tracking.ColorTracker(options.trackColors);
  const snapshots: HTMLCanvasElement[] = [];

  video.onloadeddata = () => {
    // set element height and width based on actual video stream resolution
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    video.width = video.videoWidth;
    video.height = video.videoHeight;

    // hack to maintain consistent width even though camera has different dimesions, many better solutions exists 
    $(video).parent().parent().width(video.width);
    $(video).parent().parent().height(video.height);
  }

  const promise = new Promise<HTMLCanvasElement[]>((resolve) => {

    let lastTake = new Date(); // make sure to space the snapshots out a bit to avoid them all be identical
    tracker.on('track', event => {
      if (event.data.length == 0) return;
      if (lastTake.getTime() + 200 >= new Date().getTime()) return;
      
      const best = event.data.sort(sortBySize)[0];
      const quality = getQuality(best, { leastPixels: options.leastPixels, targetRatio: options.targetRatio, ratioBound: options.ratioBound });
      
      reRenderBoundaryOnCanvas(best, canvas, snapshots.length, options.leastPixels, options.targetRatio, quality);
      if (quality != 'Good') return;
      lastTake = new Date();
      
      const crop = {
        x: best.x,  
        y: best.y,
        width: best.width,
        height: Math.min(best.height + best.height * sizeFactors.barcodeHeight, video.height - best.y)
      }

      snapshots.push(utils.takeSnapshot(video, crop));
      if (snapshots.length === options.sampleSize) {
        trackerTask.stop();

        if (video.srcObject instanceof MediaStream)
          video.srcObject!.getTracks()[0].stop();

        console.log('Track stop.');
        console.groupEnd();

        resolve(snapshots);
      }
    });

    const trackerTask = tracking.track(`${options.selector} video`, tracker, { camera: true });
    trySetBackfacingCamera(video);
  });

  return promise;
}
