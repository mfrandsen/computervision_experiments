import JSZip from 'jszip';
import { saveAs } from 'file-saver';

export const saveToDisk = async (folderName: string, imageGroups: ImageGroup[], aboutContent: Object) => {
    var zip = new JSZip();

    zip.file('README.txt', JSON.stringify(aboutContent));

    imageGroups
        .map(({ name, content }) => {
            const folder = zip.folder(name);

            content
                .map(x => ({ value: x, data: x.toDataURL('image/jpeg', 1.0) }))
                .map(x => ({ value: x.value, data: x.data.substr(x.data.indexOf(',') + 1) }))
                .forEach((x, i) => folder.file(`${i}.png`, x.data, { base64: true }));
        });

    saveAs(await zip.generateAsync({ type: "blob" }), `${folderName} (${new Date().toLocaleString("da-DA")}).zip`);
}