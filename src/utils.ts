
const round = (value: number) => Math.round(value * 1000) / 1000;

export const getRatio = (size: Size) => size.width / size.height;

export const getPixels = (size: Size) => size.height * size.width;

const isWithinRatioBound = (actual: Size, target: number, bound: Percentage) => 
    getRatioDeviationFromTarget(actual, target) <= bound;

export const getMedian = (values: number[]) => {
    const sorted = values.slice().sort(function (a, b) { return a - b; });
    var half = Math.floor(sorted.length / 2);
    if (sorted.length % 2) return sorted[half];
    else return (sorted[half - 1] + sorted[half]) / 2.0;
    // what if we get 2.5, try both, take the one with highest total confidence??
}

export const getRatioDeviationFromTarget = (actual: Size, target: number): Percentage =>
    round((Math.abs(getRatio(actual) - target) / target));

export const getQuality = (actual: Size, options: { leastPixels: number, targetRatio: number, ratioBound: Percentage }): TrackQuality => {
    const sizeAccepted = getPixels(actual) >= options.leastPixels ? true : false;
    const ratioAccepted = isWithinRatioBound(actual, options.targetRatio, options.ratioBound) ? 1 : 0;
    
    if (!sizeAccepted && !ratioAccepted) return 'BadAll';    
    else if (!sizeAccepted) return 'BadSize';
    else if (!ratioAccepted) return 'BadRatio';
    return 'Good';
}

export const sortBySize = (a: Rectangle, b: Rectangle) =>
    getPixels(b) - getPixels(a)

/**
 * Let { sub } <= { total } and 
 * let { sub } * x = total then 
 * the result is { sub' } such that { sub' } * x = actual 
 * 
 * given { sub } relative to { total }, returns { sub } relative to { actualTotal }
 */
export const scaleBy = (sub: number, actualTotal: number, total: number) =>
    sub * actualTotal / total; // multiplication before division for the sake of precision.

/**
 * given { crop } relative to { total }, 
 * returns { crop } relative to { actualTotal }  
 */
// export const transpose = (sub: Rectangle, actualTotal: Size, total: Size) => ({
//     x: scaleBy(sub.x, actualTotal.width, total.width),
//     y: scaleBy(sub.y, actualTotal.height, total.height),
//     width: scaleBy(sub.width, actualTotal.width, total.width),
//     height: scaleBy(sub.height, actualTotal.height, total.height),
// });

export const takeSnapshot = (target: HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | ImageBitmap, crop: Rectangle) => {
    /** 
     * Takes a video and a crop rectangle and returns a snapshot matching the crop object
    */
    const width = target.width;
    const height = target.height;
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d')!;

    canvas.width = width;
    canvas.height = height;
    context.drawImage(target, 0, 0, width, height)

    // BUG: for some reason we need to copy from one canvas to another for video to avoid offset miscalculations for the cropping 
    // it ought to be possible using a single canvas though
    const outputCanvas = document.createElement('canvas');
    const outputContext = outputCanvas.getContext('2d')!;
    outputCanvas.height = crop.height;
    outputCanvas.width = crop.width;
    outputContext.drawImage(canvas, crop.x, crop.y, crop.width, crop.height, 0, 0, crop.width, crop.height)

    return outputCanvas;
}

export const crop = takeSnapshot; // incidentally, it is the same implementation

export const labelToString = (label: Label) =>
    label.result == undefined   
        ? 'Unknown'
        : 'text' in label.result
            ? label.result.text // TextResult
            : label.result.codeResult && label.result.codeResult.code || 'Unknown'; // BARCODE result

export const peak = (f: () => void) => (x: any) => x;