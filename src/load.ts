const convertBlobToCanvas = (blob: Blob) =>
    new Promise<HTMLCanvasElement>(resolve => {
        const img = document.createElement('img');
        img.src = URL.createObjectURL(blob);
        img.onload = () => {

            const canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;

            const ctx = canvas.getContext('2d')!;
            ctx.drawImage(img, 0, 0)
            resolve(canvas);
        }
    });

/**
 * One might argue that it would be a prettier solution
 * if I worked with blobs throughout instead of canvas,
 * but alas that is not the current case. =p
 */
export const load = async (path: string) => {
    const response = await fetch(path);
    const blob = await response.blob();
    return await convertBlobToCanvas(blob);
};