import queryString from 'query-string';

// treat hash as the query string to avoid reloads =p
const qs = queryString.parse(location.hash.substr(1));

const getFlagValue = (flag: string) => qs[flag] === 'true';

const toggle = (flag: string) => { 
    qs[flag] = !getFlagValue(flag); 
    window.location.hash = queryString.stringify(qs); 
}

export const isTestModeEnabled = () => getFlagValue('test-mode');
export const toggleTestMode = () => toggle('test-mode');

export const isTextEnabled = () => getFlagValue('parse-text');
export const toggleParseText = () => toggle('parse-text');

export const isAzureTextEnabled = () => getFlagValue('parse-azure-text');
export const toggleAzureParseText = () => toggle('parse-azure-text');

export const isBarcodeEnabled = () => getFlagValue('parse-barcodes');
export const toggleParseBarcodes = () => toggle('parse-barcodes');

export const isGroupVisible = (key: string) => getFlagValue(`show-group-${key}`)
export const toggleGroup = (key: string) => toggle(`show-group-${key}`)

export const reload = () => window.location.reload(true);