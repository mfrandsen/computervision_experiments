// PURE ALIASES
type Percentage = number;

// GENERAL CONCEPTS
type Size = { height: number, width: number };
type Rectangle = { x: number, y: number, height: number, width: number }; 
type ImageGroup = { name: string, content: HTMLCanvasElement[] };

// DOMAIN SPECIFIC TYPES
type StringMap<T> = { [key: string] : T }
type KeyMap<TKeys, TValue> = { [K in keyof TKeys] : TValue }
type Feature = HTMLCanvasElement;
type FeatureGroup = Feature[];
type FeatureGroups = StringMap<FeatureGroup>;

type Recognizer<TLabel, TOptions> =  (featureSet: Feature[], options: TOptions) => Promise<TLabel[]>;
type RecognizerOptionsMap<TKeys, TOptions> = KeyMap<TKeys, TOptions>;

type TextLabel = { result: Tesseract.Page, confidence: number, canvas: HTMLCanvasElement }; 
type BarcodeLabel = { result: QuaggaJSResultObject, confidence: number, canvas: HTMLCanvasElement }; 
type Label = TextLabel | BarcodeLabel;
type LabelGroup = Label[];
type LabelGroups = StringMap<LabelGroup>;

type TrackQuality = 'Good' | 'BadRatio' | 'BadSize' | 'BadAll';
