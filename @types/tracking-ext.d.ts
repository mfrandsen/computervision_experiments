/// <reference path="../node_modules/@types/tracking/index.d.ts" />

declare namespace tracking {
    export function track(selector: string, tracker: tracking.Tracker, config: { camera: boolean }): TrackerTask;
} 