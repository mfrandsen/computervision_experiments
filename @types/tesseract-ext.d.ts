
/// <reference path="../node_modules/@types/tesseract.js/index.d.ts" />

interface Extension { terminate(): void; }

// declare module 'tesseract.js' {
//     interface Extension { terminate(): void; }

//     const Tesseract: Tesseract.TesseractStatic & Extension;
//     export = Tesseract;
// } 

type TesseractOptions = Object;
declare const Tesseract:  Tesseract.TesseractStatic & Extension;