/*jslint node: true */

const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractSass = new ExtractTextPlugin("styles.css");

module.exports = {
    entry: "./src/main.ts",
    mode: "development",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/
            },
            {
                test: /\.sass$/,
                use: extractSass.extract({
                    use: [
                        "css-loader", // translates CSS into CommonJS
                        "sass-loader"
                    ], // compiles Sass to CSS
                    // use style-loader in development
                    fallback: "style-loader" // creates style nodes from JS strings
                })
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js", ".sass"]
    },
    output: {  
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "build")
    },
    plugins: [extractSass]
};